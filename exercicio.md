# Exercício prático - Workshop de CI/CD

No repositório existe uma aplicação Flask e um Dockerfile. Nossa tarefa será criar uma pipeline que execute testes, faça a build e publique o artefato (imagem Docker) no Docker Registry do GitLab. Posteriormente, testaremos manualmente o uso da imagem publicada pela pipeline.

No repositório há 3 arquivos do GitLab CI:
- `.gitlab-ci.yml` - Arquivo incompleto que será usado como base para o exercício
- `.gitlab-ci.help.yml` - Arquivo com a estrutura já correta, faltando apenas os comandos corretos
- `.gitlab-ci.answer.yml` - Arquivo completo e funcional

Vamos tentar usar apenas o primeiro, e só recorrer aos outros em caso de dificuldade, ok?

## Parte 1 - Criando um GitLab Runner com Docker

1. Acesse o menu de configurações de Runners de CI/CD do seu repositório:
  - Canto inferior esquerdo
  - Settings > CI / CD
  - Acesse a seção Runners

1. Faça um pull da imagem gitlab/gitlab-runner:
  - `docker image pull gitlab/gitlab-runner`

1. Crie um container utilizando essa imagem:
  - `docker run -d --name gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner`

1. Execute o comando para registrar um novo runner:
  - `docker exec -it gitlab-runner gitlab-runner register`
  - Utilize as informações exibidas na tela de configuração do repositório para preencher a URL e o token
  - Nas tags coloque apenas `workshop`
  - No tipo de runner, `docker`
  - Na imagem default, `docker`

1. Inicie o runner com o comando:
  - `docker exec -it gitlab-runner gitlab-runner register -n --url https://gitlab.com/ --registration-token <ci_token> --executor docker --tag-list workshop --description "My Docker Runner" --docker-image docker:19.03.1 --docker-volumes /var/run/docker.sock:/var/run/docker.sock`

## Parte 2 - Pipeline: Job de Testes

1. Neste exemplo, faremos apenas a validação do código utilizando o linter de código Python `pylint`

1. Abra o arquivo `.gitlab-ci.yml`, e no espaço adequado, inicie a implementação do job de testes:
  - Para a execução desse teste, utilize a imagem `python:3`
  - Antes da execução do script, devemos instalar o pacote do `pylint` utilizando o `pip3` (comando `pip3 install pylint`)
  - Utilize o `pylint` para validar o código da aplicação (comando `pylint app.py`)

1. Se necessário, utilize a documentação do GitLab CI: https://docs.gitlab.com/ee/ci/yaml/

1. Após as alterações, faça um push do código
  - `git add .`
  - `git commit -m "Test job"`
  - `git push origin master`

1. Acompanhe a execução do job, e em caso de falha, corrija o erro e faça um novo push

## Parte 3 - Pipeline: Job de Build do artefato

1. Para fazer a build do artefato, utilize o comando `docker build -t ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/ci-cd-workshop:${CI_COMMIT_SHORT_SHA} app/`

1. Após as alterações, faça um push do código
  - `git add .`
  - `git commit -m "Build job"`
  - `git push origin master`

1. Acompanhe a execução do job, e em caso de falha, corrija o erro e faça um novo push

## Parte 4 - Pipeline: Job de publicação do artefato

1. Para publicar a imagem que fizemos o build precisamos, antes de tudo, fazer login no Docker Registry do GitLab
  1. Para fazer login, utilizamos o comando `docker login -u <username> -p <password> registry.gitlab.com/<username>`. porém, conseguimos utilizar o token do job, e usar o comando `echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin ${CI_REGISTRY}`

1. Depois de fazer o login, utilize o comando para fazer a publicação do artefato: `docker push ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/ci-cd-workshop:${CI_COMMIT_SHORT_SHA}`

## Parte 5 - Testando o deploy

1. Normalmente, uma pipeline é capaz de fazer o trigger do deploy, porém nosso ambiente é limitado, e não conseguiremos vê-lo ocorrer de forma automática

1. Para testar localmente a imagem que você fez o build, execute localmente:
  - `docker login -u <username> registry.gitlab.com/<username>`
  - `docker image pull registry.gitlab.com/<username>/ci-cd-workshop:<HASH_DO_COMMIT>` (pode ser encontrado na interface do GitLab)
  - `docker run -d --name workshop-container -p 5000:5000 registry.gitlab.com/<username>/ci-cd-workshop:<HASH_DO_COMMIT>`
  - Acesse no browser o endereço `localhost:5000`

## Bônus - Experimentando o CI/CD

1. Crie uma nova branch e faça alterações na aplicação (por exemplo, a mensagem exibida), veja como a pipeline publica uma nova imagem com o novo código a cada commit feito

1. Use sua imaginação e implemente validações e lógicas na pipeline, como por exemplo:
  - Publicar a imagem apenas ao criar uma tag
  - Fazer build da imagem apenas na branch master
  - Em branches publicar usando o commit hash como versão, mas nas tags utilizar o nome da tag
